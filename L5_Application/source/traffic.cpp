#include "examples/examples.hpp"
#include "LPC17xx.h"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
#include "led/CMPE244/paneltext.h"
#include "math.h"
#include "stdio.h"
#include "string.h"
#include "timers.h"
#include "gpio.hpp"
#include "io.hpp"
#include "vehicle.h"
#include "traffic.hpp"


uint8_t collision[64][64]={0};
RGB &Traffic =RGB::getRGBInstance();
traffic::traffic(uint8_t lane)
{
    position=0;
    speed = (rand() % (5+1 - 1)+1);
    Lane = lane;


    random_traffic=(rand() % (7+1 - 0)+0);
    vehicle_update();
    offset=(rand() % (55+1 - 35)+35);
    switch(Lane)
    {
        case 1:
            offset=(rand() % (15+1 - 6)+6);
            break;
        case 2:
            offset=(rand() % (28+1 - 19)+19);
            break;
        case 3:
            offset=(rand() % (41+1 - 32)+32);
            break;
        case 4:
            offset=(rand() % (55+1 - 45)+45);
            break;

    };
}

void traffic::reset_position()
{
    position=64;

    for(int i = 0 ; i < 64 ; i++ )
    {
        for(int j = 0 ; j<64 ; j++)
        {
            collision[j][i]=0;
        }
    }
}


void traffic::vehicle_update()
{
    switch (random_traffic)
    {
        case 0:
            vehicle_width=3;
            vehicle_length=5;
            vehicle_type = &car_1[0][0];
            break;
        case 1:
            vehicle_width=3;
            vehicle_length=5;
            vehicle_type = &car_2[0][0];
            break;
        case 2:
            vehicle_width=3;
            vehicle_length=5;
            vehicle_type = &racing[0][0];
            break;

        case 3:
            vehicle_width=4;
            vehicle_length=12;
            vehicle_type = &truckc[0][0];

            break;
        case 4:
            vehicle_width=5;
            vehicle_length=12;
            vehicle_type = &truckm[0][0];

            break;
        case 5:
            vehicle_width=4;
            vehicle_length=12;
            vehicle_type = &truckp[0][0];

            break;
        case 6:
            vehicle_width=4;
            vehicle_length=12;
            vehicle_type = &trucke[0][0];

            break;
        case 7:
            vehicle_width=1;
            vehicle_length=3;
            vehicle_type = &bike[0][0];


    };
}

void traffic::update(uint8_t level)
{
    // position = Position;
    position=position+speed;
    if(position >= 64)
    {
        position=0;
        switch(Lane)
        {
            case 1:
                offset=(rand() % (15+1 - 6)+6);
                break;
            case 2:
                offset=(rand() % (28+1 - 19)+19);
                break;
            case 3:
                offset=(rand() % (41+1 - 32)+32);
                break;
            case 4:
                offset=(rand() % (54+1 - 45)+45);
                break;

        }

        random_traffic = (rand() % (7+1 - 0)+0);

        switch(level)
        {
            case 1:
                speed = (rand() % (3+1 - 1)+1);
                break;
            case 2:
                speed = (rand() % (4+1 - 1)+1);
                break;
            case 3:
                speed = (rand() % (4+1 - 2)+2);
                break;
            case 10:
                speed = 6;
                break;
            default:
                speed = (rand() % (5+1 - 3)+3);
                break;

        }

    }
}
void traffic::draw()
{

    int count=0;
    //  vehicle_type = &truckc[0][0];
    vehicle_update();
    for(int i = 0 ; i<vehicle_length ; i++)
    {

        for(int j = 0 ; j <vehicle_width ; j++)
        {
            Traffic.drawPixel(j+offset,position+i,vehicle_type[j]);
            collision[j+offset][position+i]|=1;


        }
        vehicle_type=vehicle_type+vehicle_width;
    }
}

void traffic::clear()
{

    for(int j = 0 ; j < vehicle_width ; j++)
    {
        for(int s=0 ; s<speed ; s++)
        {
            Traffic.drawPixel(j+offset,position+s,0);
            collision[j+offset][position+s]=0;
        }

    }
}
