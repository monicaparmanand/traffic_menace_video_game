/*
 * level_score.h
 *
 *  Created on: May 1, 2018
 *      Author: Dheemanth
 */

#ifndef LEVEL_SCORE_H_
#define LEVEL_SCORE_H_


#include "tasks.hpp"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
#include "collision.h"
#include "io.hpp"
#include  "user_car.h"




GPIO test(P0_0);
TaskHandle_t level_handle=NULL;
RGB &score =RGB::getRGBInstance();
int player_score=0;
volatile bool boost=false;
int level=1;
int lives_count=0;


void set_car_position(void)
{
    switch(level)
    {
        case 1:
        case 2:
        case 3:
            car_position=55;
            break;
        case 4:
        case 5:
            car_position=46;
            break;
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
            car_position=46;
            break;
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
            car_position=37;
            break;

    }
}



void level_score(void *p){
    test.setAsInput();
    int timer=0;
    while(1)
    {


        score.setCursor(0,0);
        score.setTextColor(0xF000,0x0);
        score.print("L\nV\nL\n");
        score.drawPixel(1,25,0xFFFF);
        score.drawPixel(3,25,0xFFFF);

        if(level<10)
        {
            score.setCursor(0,27);
            score.print(level%10);
        }
        else if(level>10)
        {
            score.setCursor(0,27);
            score.print(level/10);
            score.setCursor(0,34);
            score.print(level%10);

        }

        if(lives_count<3)
        {
            switch(lives_count)
            {
                case 0:
                    score.setCursor(0,40);
                    score.print('*');
                    score.setCursor(0,48);
                    score.print('*');
                    score.setCursor(0,56);
                    score.print('*');
                    break;
                case 1:
                    score.setCursor(0,40);
                    score.print('*');
                    score.setCursor(0,48);
                    score.print('*');
                    break;
                case 2:
                    score.setCursor(0,40);
                    score.print('*');
                    break;

            }
        }



        score.setCursor(59,0);
        score.setTextColor(0xF000,0x000);
        score.print("S");
        score.setCursor(59,8);
        score.print("C");
        score.setCursor(59,16);
        score.print("O");
        score.setCursor(59,24);
        score.print("R");
        score.setCursor(59,32);
        score.print("E");

        score.drawPixel(60,40,0xFFFF);
        score.drawPixel(62,40,0xFFFF);

        if(player_score<10)
        {
            score.setCursor(59,41);
            score.print(player_score%10);
        }
        else if(player_score <100)
        {
            score.setCursor(59,41);
            score.print(player_score/10);
            score.setCursor(59,49);
            score.print(player_score%10);

        }
        else
        {
            score.setCursor(59,41);
            score.print(player_score/100);
            score.setCursor(59,49);
            score.print((player_score/10)%10);
            score.setCursor(59,57);
            score.print(player_score%10);

        }


        vTaskDelay(1000);
        timer++;

        if(timer %15 == 0)
        {
            level+=1;

            for(int i = 0 ; i <5; i++)
            {
                for(int j = 0 ; j < 3 ; j++)
                {
                    score.drawPixel(i+car_x,j+car_position,0x0);
                    collision[i+car_x][j+car_position]=0;


                }
            }
            set_car_position();
        }

        if(boost)
        {
            player_score+=5;
        }
        else
        {
            switch(level)
            {
                case 1: player_score+=1;
                break;
                case 2: player_score+=2;
                break;
                case 3: player_score+=3;
                break;
                default :player_score+=4;
                break;
            }
        }

        vTaskDelay(10);
    }
}



#endif /* LEVEL_SCORE_H_ */
