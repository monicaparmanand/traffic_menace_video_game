/*
 * traffic.hpp
 *
 *  Created on: Apr 23, 2018
 *      Author: Dheemanth
 */

#ifndef TRAFFIC_HPP_
#define TRAFFIC_HPP_

#include "inttypes.h"

extern uint8_t collision[64][64];

class traffic
{
public:

    enum vehicle_types
    {
        car_small,
        car_small_2,
        racing_car,
        truck_C,
        truck_M,
        truck_P,
        truck_E

    };

    traffic(uint8_t lane);
    void update(uint8_t level);
    void vehicle_update();
    void draw();
    void clear();
    void reset_position();





private:
    uint8_t vehicle_width;
    uint8_t vehicle_length;
    uint8_t position;
    uint8_t speed;
    uint16_t *vehicle_type;
    uint8_t offset;
    uint8_t Lane;
    uint8_t random_traffic;

};



#endif /* TRAFFIC_HPP_ */
