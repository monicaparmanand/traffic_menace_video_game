/*
 * welcome_task.h
 *
 *  Created on: May 1, 2018
 *      Author: Dheemanth
 */

#ifndef WELCOME_TASK_H_
#define WELCOME_TASK_H_



#include "tasks.hpp"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
#include "collision.h"
#include "level_score.h"
#include "traffic_lane.h"
#include "user_car.h"
#include "road.h"


TaskHandle_t welcome_handle=NULL;


RGB &welcome =RGB::getRGBInstance();

int start_count=3;

void welcome_task(void *p){

    while(1)
    {

        vTaskSuspend(level_handle);
        vTaskSuspend(road_handle);
        vTaskSuspend(traffic_handle);
        vTaskSuspend(crash_handle);
        vTaskSuspend(restart_game);

        printf("welcome_resumed");
        welcome.setTextColor(0xFFFF,0x0);
        welcome.setCursor(12,10);
        welcome.print("WELCOME\n");
        welcome.setCursor(27,20);
        welcome.print("TO\n ");
        welcome.setCursor(12,30);
        welcome.print("TRAFFIC\n");
        welcome.setCursor(12,40);
        welcome.print("MENANCE\n");
        welcome.setCursor(27,50);
        welcome.setTextColor(0xF000,0x0);
        welcome.print(start_count);
        start_count--;
        vTaskDelay(1000);
        welcome.setCursor(27,50);
        welcome.print(start_count);
        start_count--;
        vTaskDelay(1000);
        welcome.setCursor(27,50);
        welcome.print(start_count);
        start_count--;
        start_count=3;
        /*        vTaskDelay(1000);
        display.setCursor(27,50);
        display.print(start_count);
        start_count--;
        vTaskDelay(1000);
        display.setCursor(27,50);
        display.print(start_count);
        start_count--;*/

        welcome.fillScreen(0x00);
        vTaskResume(road_handle);
        vTaskResume(traffic_handle);
        vTaskResume(level_handle);
        vTaskResume(crash_handle);
        vTaskResume(restart_game);

        lives_count=0;
        player_score=0;
        level=1;
        vTaskSuspend(welcome_handle);
    }
}



#endif /* WELCOME_TASK_H_ */
