/*
 * road.h
 *
 *  Created on: May 1, 2018
 *      Author: Dheemanth
 */

#ifndef ROAD_H_
#define ROAD_H_


#include "tasks.hpp"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
volatile bool is_pwer_up=0;




TaskHandle_t road_handle=NULL;


RGB &display_road =RGB::getRGBInstance();


void road(void *p){


    while(1){

        //  display_road.fillRect(0,0,5,64,0xFFFF);
        display_road.drawFastVLine(6,0,64,0xFFFF);
        // display_road.fillRect(58,0,5,64,0xFFFF);
        display_road.drawFastVLine(58,0,64,0xFFFF);
        if(is_pwer_up)
        {

            display_road.drawFastVLine(6,0,65,0xFF000);
            display_road.drawFastVLine(58,0,65,0xFF000);
        }

        //LANE1
        display_road.fillRect(18,0,1,4,0xFFFF);
        display_road.fillRect(18,10,1,4,0xFFFF);
        display_road.fillRect(18,20,1,4,0xFFFF);
        display_road.fillRect(18,30,1,4,0xFFFF);
        display_road.fillRect(18,40,1,4,0xFFFF);
        display_road.fillRect(18,50,1,4,0xFFFF);
        display_road.fillRect(18,60,1,4,0xFFFF);

        //LANE2
        display_road.fillRect(31,0,1,4,0xFFFF);
        display_road.fillRect(31,10,1,4,0xFFFF);
        display_road.fillRect(31,20,1,4,0xFFFF);
        display_road.fillRect(31,30,1,4,0xFFFF);
        display_road.fillRect(31,40,1,4,0xFFFF);
        display_road.fillRect(31,50,1,4,0xFFFF);
        display_road.fillRect(31,60,1,4,0xFFFF);

        //LANE3
        display_road.fillRect(44,0,1,4,0xFFFF);
        display_road.fillRect(44,10,1,4,0xFFFF);
        display_road.fillRect(44,20,1,4,0xFFFF);
        display_road.fillRect(44,30,1,4,0xFFFF);
        display_road.fillRect(44,40,1,4,0xFFFF);
        display_road.fillRect(44,50,1,4,0xFFFF);
        display_road.fillRect(44,60,1,4,0xFFFF);

        vTaskDelay(10);

    }
}





#endif /* ROAD_H_ */
