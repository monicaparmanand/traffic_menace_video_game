/*
 * user_car.h
 *
 *  Created on: May 1, 2018
 *      Author: Dheemanth
 */

#ifndef USER_CAR_H_
#define USER_CAR_H_


#include "tasks.hpp"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
#include "io.hpp"


TaskHandle_t crash_handle=NULL;

RGB &crash_user =RGB::getRGBInstance();

int car_x=6;
int car_prev;
int car_y;
volatile int car_position=55;



uint16_t our_Car[5][3] =
{

        0x0, 0,0x0,
        0xf780, 0x0,0xf780,
        0x0,0xF000,0x0,
        0xf780,0,0xf780,
        0,0,0
};

void crash(void *p)
{
    while(1)
    {


        if(AS.getX()>150)
        {
            LE.on(1); //RIGHT
            car_prev=car_x;
            car_x+=1;
            if(car_x >= 55) car_x = 54;
            crash_user.fillRect(car_prev,car_position,2,2,0x0);
        }

        if(AS.getX()<-150)
        {
            LE.on(4);   //LEFT
            car_prev=car_x;
            car_x-=1;
            if(car_x <=8) car_x = 6;
            crash_user.fillRect(car_prev,car_position,2,2,0x0);

        }



        //Car position update
        for(int i = 0 ; i <5; i++)
        {
            for(int j = 0 ; j < 3 ; j++)
            {
                crash_user.drawPixel(i+car_x,j+car_position,our_Car[i][j]);
                collision[i+car_x][j+car_position]|=2;


            }
        }
        vTaskDelay(10);
        for(int i = 0 ; i <5; i++)
        {
            for(int j = 0 ; j < 3 ; j++)
            {
                crash_user.drawPixel(i+car_x,j+car_position,0x0);
                collision[i+car_x][j+car_position]=0;


            }
        }



        vTaskDelay(5);

    }
}




#endif /* USER_CAR_H_ */
