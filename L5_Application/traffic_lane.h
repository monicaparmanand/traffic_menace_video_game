/*
 * traffic_lane.h
 *
 *  Created on: May 1, 2018
 *      Author: Dheemanth
 */

#ifndef TRAFFIC_LANE_H_
#define TRAFFIC_LANE_H_


#include "tasks.hpp"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
#include "level_score.h"
#include "traffic.hpp"



TaskHandle_t traffic_handle=NULL;


traffic tr(1);
traffic cr(2);
traffic dr(3);
traffic er(4);


void traffic_task_1(void *p){

    while(1){


        tr.update(level);
        tr.draw();

        cr.update(level);
        cr.draw();

        dr.update(level);
        dr.draw();

        er.update(level);
        er.draw();

        vTaskDelay(75);

        tr.clear();
        dr.clear();
        cr.clear();
        er.clear();



        vTaskDelay(2);
    }
}




#endif /* TRAFFIC_LANE_H_ */
