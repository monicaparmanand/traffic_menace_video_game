#ifndef RGB_h
#define RGB_h

#include "mfGFX.h"

class RGB : public GFX {

public:

    static RGB& getRGBInstance();
    static RGB* rgbInstance;
    // Constructor for 16x32 panel:
   // RGB(uint32_t a, uint32_t b, uint32_t c,uint32_t d,uint32_t e,uint8_t sclk, uint32_t latch, uint8_t oe, bool dbuf, uint8_t width=32);

    // Constructor for 32x32 panel (adds 'd' pin):
    //RGB(uint32_t a, uint32_t b, uint32_t c, uint8_t d,uint8_t sclk, uint32_t latch, uint8_t oe, bool dbuf, uint8_t width=32);

    void
       begin(void),
       drawPixel(uint16_t x, uint16_t y, uint16_t c),
       fillScreen(uint16_t c),
       updateDisplay(void),
       swapBuffers(bool);
     uint8_t *backBuffer(void);
     uint16_t
       ColorHSV(long hue, uint8_t sat, uint8_t val, bool gflag);

     void init(uint8_t rows, uint32_t a, uint32_t b, uint32_t c,uint32_t d , uint32_t e,
       uint8_t sclk, uint32_t latch, uint8_t oe, bool dbuf,
       uint8_t width);
    private:

     uint8_t         *matrixbuff[2];
     uint8_t          nRows;
     volatile uint8_t backindex;
     volatile bool swapflag;
     RGB();



     uint8_t   _sclk, _latch, _oe, _a, _b, _c, _d,_e;

     volatile uint8_t row, plane;
     volatile uint8_t *buffptr;
   };

   #endif
