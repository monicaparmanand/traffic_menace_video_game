#include "RGB.h"
#include "gamma.h"
#include "stdio.h"
#include "string.h"
#include "LPC17xx.h"
#include "soft_timer.hpp"
#include "tasks.hpp"
#include "lpc_sys.h"
#include "lpc_rit.h"


void refreshISR(void);

#define nPlanes 2

#define R1	(1<<0)		// bit 2 = RED 1 	P2.0
#define G1	(1<<1)		// bit 3 = GREEN 1	P2.1
#define B1	(1<<2)		// bit 4 = BLUE 1	P2.2
#define R2	(1<<3)		// bit 5 = RED 2	P2.3
#define G2	(1<<4)		// bit 6 = GREEN 2	P2.4
#define B2	(1<<5)		// bit 7 = BLUE 2	P2.5

static RGB *activePanel = NULL;
volatile uint8_t mybuff[2048]={0};
RGB* RGB::rgbInstance=0;

extern "C" void TIMER0_IRQHandler (void)
{
    if((LPC_TIM0->IR & 0x01) == 0x01) // if MR0 interrupt
    {
        LPC_TIM0->IR |= 1 << 0; // Clear MR0 interrupt flag
        refreshISR();
    }

}


RGB::RGB():GFX(64,64){

}

RGB& RGB::getRGBInstance()
{
if(rgbInstance==0)
{
    rgbInstance=new RGB();
}
return *rgbInstance;
}

void RGB::init(uint8_t rows, uint32_t a, uint32_t b, uint32_t c, uint32_t d , uint32_t e,uint8_t sclk, uint32_t latch, uint8_t oe, bool dbuf, uint8_t width2) {
  nRows = rows; // Number of multiplexed rows; actual height is 2X this
  // Allocate and initialize matrix buffer:
  int buffsize  = width2 * nRows * 3, // x3 = 3 bytes holds 4 planes "packed"
      allocsize = (dbuf == true) ? (buffsize * 2) : buffsize;
  if(NULL == (matrixbuff[0] = (uint8_t *)malloc(allocsize))) return;
  memset(matrixbuff[0], 0, allocsize);

  matrixbuff[1] = (dbuf == true) ? &matrixbuff[0][buffsize] : matrixbuff[0];

  // Save pin numbers for use by begin() method later.
  _a     = a;
  _b     = b;
  _c     = c;
  _d     = d;
  _e     = e;
  _sclk  = sclk;
  _latch = latch;
  _oe    = oe;

  plane     = nPlanes - 1;
  row       = nRows   - 1;
  swapflag  = false;
  backindex = 0;     // Array index of back buffer


  LPC_GPIO1->FIODIR |= (1<<_a);
  LPC_GPIO1->FIODIR |= (1<<_b);
  LPC_GPIO1->FIODIR |= (1<<_c);
  LPC_GPIO1->FIODIR |= (1<<_d);
  LPC_GPIO1->FIODIR |= (1<<_e);

  LPC_GPIO1->FIODIR |= (1<<_latch);

  LPC_GPIO2->FIODIR |= (1<<_sclk);
  LPC_GPIO2->FIODIR |= (1<<_oe);
  LPC_GPIO2->FIODIR |= R1;
  LPC_GPIO2->FIODIR |= G1;
  LPC_GPIO2->FIODIR |= B1;
  LPC_GPIO2->FIODIR |= R2;
  LPC_GPIO2->FIODIR |= G2;
  LPC_GPIO2->FIODIR |= B2;

  LPC_GPIO2->FIOSET = (1<<_oe);
  LPC_GPIO2->FIOCLR = (1<<_sclk);
  LPC_GPIO1->FIOCLR = (1<<_a);
  LPC_GPIO1->FIOCLR = (1<<_b);
  LPC_GPIO1->FIOCLR = (1<<_c);
  LPC_GPIO1->FIOCLR = (1<<_d);
  LPC_GPIO1->FIOCLR = (1<<_e);
  LPC_GPIO1->FIOCLR = (1<<_latch);
  LPC_GPIO2->FIOCLR = R1;
  LPC_GPIO2->FIOCLR = G1;
  LPC_GPIO2->FIOCLR = B1;
  LPC_GPIO2->FIOCLR = R2;
  LPC_GPIO2->FIOCLR = G2;
  LPC_GPIO2->FIOCLR = B2;

}


void RGB::begin(void) {

  backindex   = 0;                         // Back buffer
  buffptr     = matrixbuff[1 - backindex]; // -> front buffer
  activePanel = this;                      // For interrupt hander



  LPC_SC->PCONP |= 1 << 1; //Power up Timer 0
  LPC_SC->PCLKSEL0 |= 1 << 2; // Clock for timer = CCLK
  LPC_TIM0->MR0 = 1 << 13; // Give a value suitable for the LED blinking frequency based on the clock frequency
  LPC_TIM0->MCR |= 1 << 0; // Interrupt on Match0 compare
  LPC_TIM0->MCR |= 1 << 1; // Reset timer on Match 0.
  LPC_TIM0->TCR |= 1 << 1; // Manually Reset Timer0 ( forced )
  LPC_TIM0->TCR &= ~(1 << 1); // stop resetting the timer.
  NVIC_EnableIRQ(TIMER0_IRQn); // Enable timer interrupt
  LPC_TIM0->TCR |= 1 << 0; // Start timer


}





uint16_t RGB::ColorHSV(
  long hue, uint8_t sat, uint8_t val, bool gflag) {

  uint8_t  r, g, b, lo;
  uint16_t s1, v1;

  // Hue
  hue %= 1536;             // -1535 to +1535
  if(hue < 0) hue += 1536; //     0 to +1535
  lo = hue & 255;          // Low byte  = primary/secondary color mix
  switch(hue >> 8) {       // High byte = sextant of colorwheel
    case 0 : r = 255     ; g =  lo     ; b =   0     ; break; // R to Y
    case 1 : r = 255 - lo; g = 255     ; b =   0     ; break; // Y to G
    case 2 : r =   0     ; g = 255     ; b =  lo     ; break; // G to C
    case 3 : r =   0     ; g = 255 - lo; b = 255     ; break; // C to B
    case 4 : r =  lo     ; g =   0     ; b = 255     ; break; // B to M
    default: r = 255     ; g =   0     ; b = 255 - lo; break; // M to R
  }


  s1 = sat + 1;
  r  = 255 - (((255 - r) * s1) >> 8);
  g  = 255 - (((255 - g) * s1) >> 8);
  b  = 255 - (((255 - b) * s1) >> 8);


  v1 = val + 1;
  if(gflag) { // Gamma-corrected color?
    r = gamm[(r * v1) >> 8]; // Gamma correction table maps
    g = gamm[(g * v1) >> 8]; // 8-bit input to 4-bit output
    b = gamm[(b * v1) >> 8];
  } else { // linear (uncorrected) color
    r = (r * v1) >> 12; // 4-bit results
    g = (g * v1) >> 12;
    b = (b * v1) >> 12;
  }
  return (r << 12) | ((r & 0x8) << 8) | // 4/4/4 -> 5/6/5
         (g <<  7) | ((g & 0xC) << 3) |
         (b <<  1) | ( b        >> 3);
}

void RGB::drawPixel(uint16_t x, uint16_t y, uint16_t c) {
  uint8_t r, g, b;
  volatile uint8_t *ptr;

  if((x < 0) || (x >= _width) || (y < 0) || (y >= _height)) return;

  switch(rotation) {
   case 1:
    swap(x, y);
    x = WIDTH  - 1 - x;
    break;
   case 2:
    x = WIDTH  - 1 - x;
    y = HEIGHT - 1 - y;
    break;
   case 3:
    swap(x, y);
    y = HEIGHT - 1 - y;
    break;
  }

  r =  (c >> 12) & 0xF;        // RRRRrggggggbbbbb
  g = (c >>  8) & 0xF; // rrrrGGGGggbbbbb
  b = (c >>  1) & 0xF; // rrrrgggggggBBBBb

  if(y < nRows) {

    ptr = &mybuff[y * WIDTH  + x]; // Base addr

    //for(; bit < limit; bit <<= 1) {
      *ptr &= ~0B00011100;            // Mask out R,G,B in one op
      if(r & 1) *ptr |= 0B00000100; // Plane N R: bit 2
      if(g & 1) *ptr |= 0B00001000; // Plane N G: bit 3
      if(b & 1) *ptr |= 0B00010000; // Plane N B: bit 4
      //ptr  += WIDTH;                 // Advance to next bit plane
    //}
  } else {

    ptr = &mybuff[(y - nRows) * WIDTH + x];

    *ptr &= ~0B11100000;            // Mask out R,G,B in one op
      if(r & 1) *ptr |= 0B00100000; // Plane N R: bit 5
      if(g & 1) *ptr |= 0B01000000; // Plane N G: bit 6
      if(b & 1) *ptr |= 0B10000000; // Plane N B: bit 7



  }
}

void RGB::fillScreen(uint16_t c) {

    GFX::fillScreen(c);

}

// Return address of back buffer -- can then load/store data directly
uint8_t *RGB::backBuffer() {
  return matrixbuff[backindex];
}

void RGB::swapBuffers(bool copy) {
  if(matrixbuff[0] != matrixbuff[1]) {

    swapflag = true;                  // Set flag here, then...
    while(swapflag == true) ;//delay(1); // wait for interrupt to clear it
    if(copy == true)
      memcpy(matrixbuff[backindex], matrixbuff[1-backindex], WIDTH * nRows);
  }
}


void refreshISR(void)
{

  activePanel->updateDisplay();   // Call refresh func for active display
}

void RGB::updateDisplay(void) {
  uint8_t  i, *ptr;



  LPC_GPIO2->FIOCLR = (1<<_sclk);
  LPC_GPIO2->FIOSET = (1<<_oe);

  LPC_GPIO1->FIOSET = (1<<_latch);



  if(++plane >= nPlanes) {      // Advance plane counter.  Maxed out?
    plane = 0;                  // Yes, reset to plane 0, and

  if(++row >= nRows) {

      row     = 0;              // Yes, reset row counter, then...


      buffptr = &mybuff[0]; // Reset into front buffer
    }
  } else if(plane == 1) {


	  (row & 0x1) ? LPC_GPIO1->FIOSET = (1<<_a) : LPC_GPIO1->FIOCLR = (1<<_a);
	  (row & 0x2) ? LPC_GPIO1->FIOSET = (1<<_b) : LPC_GPIO1->FIOCLR = (1<<_b);
	  (row & 0x4) ? LPC_GPIO1->FIOSET = (1<<_c) : LPC_GPIO1->FIOCLR = (1<<_c);
	  (row & 0x8) ? LPC_GPIO1->FIOSET = (1<<_d) : LPC_GPIO1->FIOCLR = (1<<_d);
	  (row & 0x10) ? LPC_GPIO1->FIOSET = (1<<_e) : LPC_GPIO1->FIOCLR = (1<<_e);
     }


  ptr = (uint8_t *)buffptr;


  LPC_GPIO1->FIOCLR = (1<<_latch);
  LPC_GPIO2->FIOCLR = (1<<_oe);

  if(plane > 0) {
    for (i=0; i < WIDTH; i++) {

		(ptr[i] & 0x04) ? LPC_GPIO2->FIOSET = R1 : LPC_GPIO2->FIOCLR = R1; 	//R1
		(ptr[i] & 0x08) ? LPC_GPIO2->FIOSET = G1 : LPC_GPIO2->FIOCLR = G1;	//G1
		(ptr[i] & 0x10) ? LPC_GPIO2->FIOSET = B1 : LPC_GPIO2->FIOCLR = B1; 	//B1
		(ptr[i] & 0x20) ? LPC_GPIO2->FIOSET = R2 : LPC_GPIO2->FIOCLR = R2;	 //R2
		(ptr[i] & 0x40) ? LPC_GPIO2->FIOSET = G2 : LPC_GPIO2->FIOCLR = G2;	 //G2
		(ptr[i] & 0x80) ? LPC_GPIO2->FIOSET = B2 : LPC_GPIO2->FIOCLR = B2;	 //B2
		LPC_GPIO2->FIOSET = (1<<_sclk);
		LPC_GPIO2->FIOCLR = (1<<_sclk);
	}

    buffptr += WIDTH;


  } else {


	for(i=0; i<WIDTH; i++) {


		(ptr[i] & 0x04) ? LPC_GPIO2->FIOSET = R1 : LPC_GPIO2->FIOCLR = R1; 	//R1
		(ptr[i] & 0x08) ? LPC_GPIO2->FIOSET = G1 : LPC_GPIO2->FIOCLR = G1;	//G1
		(ptr[i] & 0x10) ? LPC_GPIO2->FIOSET = B1 : LPC_GPIO2->FIOCLR = B1; 	//B1
		(ptr[i] & 0x20) ? LPC_GPIO2->FIOSET = R2 : LPC_GPIO2->FIOCLR = R2;	 //R2
		(ptr[i] & 0x40) ? LPC_GPIO2->FIOSET = G2 : LPC_GPIO2->FIOCLR = G2;	 //G2
		(ptr[i] & 0x80) ? LPC_GPIO2->FIOSET = B2 : LPC_GPIO2->FIOCLR = B2;	 //B2
		LPC_GPIO2->FIOSET = (1<<_sclk);
		LPC_GPIO2->FIOCLR = (1<<_sclk);


    }
  }


}

