/*
 * collision.h
 *
 *  Created on: May 1, 2018
 *      Author: Dheemanth
 */

#ifndef COLLISION_H_
#define COLLISION_H_

#include "tasks.hpp"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
#include "traffic.hpp"
#include "traffic_lane.h"
#include "user_car.h"
#include "level_score.h"
#include "road.h"

volatile bool game_over_flag=0;




RGB &c =RGB::getRGBInstance();



TaskHandle_t colllision_handle=NULL;
TaskHandle_t restart_game=NULL;


void collision_task(void *p)
{

    while(1)
    {


        for(int i=0 ; i < 64 ; i++)
        {
            if(collision[i][car_position] >2 || collision[i][car_position+3] >2)
            {
                lives_count++;

                switch(lives_count)
                {
                    case(1):
                    vTaskSuspend(level_handle);
                    vTaskSuspend(road_handle);
                    vTaskSuspend(traffic_handle);
                    vTaskSuspend(crash_handle);
                    vTaskDelay(1000);
                    c.fillScreen(0x00);
                    c.setTextSize(2);
                    c.setCursor(20,20);
                    c.print("**");

                    tr.reset_position();
                    cr.reset_position();
                    dr.reset_position();
                    er.reset_position();
                    c.setTextSize(1);
                    vTaskDelay(500);
                    c.fillScreen(0x00);
                    vTaskResume(road_handle);
                    vTaskResume(traffic_handle);
                    vTaskResume(level_handle);
                    vTaskResume(crash_handle);
                    break;

                    case(2):
                    vTaskSuspend(level_handle);
                    vTaskSuspend(road_handle);
                    vTaskSuspend(traffic_handle);
                    vTaskSuspend(crash_handle);

                    vTaskDelay(1000);
                    c.fillScreen(0x00);
                    c.setTextSize(2);
                    c.setCursor(20,20);
                    c.print("*");

                    tr.reset_position();
                    cr.reset_position();
                    dr.reset_position();
                    er.reset_position();
                    c.setTextSize(1);
                    vTaskDelay(500);
                    c.fillScreen(0x00);
                    vTaskResume(road_handle);
                    vTaskResume(traffic_handle);
                    vTaskResume(level_handle);
                    vTaskResume(crash_handle);


                    break;




                    case(3):
                                                            vTaskSuspend(level_handle);
                    vTaskSuspend(road_handle);
                    vTaskSuspend(traffic_handle);
                    vTaskSuspend(crash_handle);

                    vTaskDelay(1000);
                    c.fillScreen(0x00);

                    game_over_flag=1;
                    is_pwer_up=0;
                    c.setCursor(6,8);
                    c.print("GAME OVER\n");
                    c.setCursor(24,16);
                    c.print("YOU");
                    c.setCursor(12,24);
                    c.print("CRASHED");
                    c.setCursor(8,35);
                    c.print("SCORE:");
                    c.print(player_score);

                    tr.reset_position();
                    cr.reset_position();
                    dr.reset_position();
                    er.reset_position();

                    vTaskResume(restart_game);
                    vTaskDelay(2000);



                }

            }
        }

    }
}




#endif /* COLLISION_H_ */
