/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 * 			FreeRTOS and stdio printf is pre-configured to use uart0_min.h before main() enters.
 * 			@see L0_LowLevel/lpc_sys.h if you wish to override printf/scanf functions.
 *
 */
#include "tasks.hpp"
#include "examples/examples.hpp"
#include "LPC17xx.h"
#include "led/CMPE244/mfGFX.h"   // Core graphics library
#include "led/CMPE244/RGB.h"
#include "led/CMPE244/paneltext.h"
#include "math.h"
#include "stdio.h"
#include "string.h"
#include "timers.h"
#include "gpio.hpp"
#include "io.hpp"
#include "vector.hpp"
#include "traffic.hpp"
#include "printf_lib.h"
#include "welcome_task.h"
#include "collision.h"
#include "traffic_lane.h"
#include "user_car.h"
#include "level_score.h"
#include "eint.h"

void restart_func(void);


/*
#define CLK 6
#define OE  7
#define LAT 29
#define A   30
#define B   20
#define C   22
#define D   23
#define E   31
 */

GPIO pin(P0_0);

#define CLK 6
#define OE  7
#define LAT 29
#define A   19
#define B   20
#define C   22
#define D   23
#define E   28

RGB &display =RGB::getRGBInstance();
GPIO res(P0_26);

int prev_level;

void restart_func(void)
{
    //clear car posotions
    for(int i = 0 ; i <5; i++)
    {
        for(int j = 0 ; j < 3 ; j++)
        {
            display.drawPixel(i+car_x,j+car_position,0x0);
            collision[i+car_x][j+car_position]=0;


        }
    }

    if(level <= 10){
        car_position=car_position-9;
        boost=true;}



}

void game_res(void)
{
    if(game_over_flag)
    {
        display.fillScreen(0);
        start_count=3;
        vTaskResume(welcome_handle);
        vTaskSuspend(restart_game);
        game_over_flag=0;

    }
}
void blank(void)
{

    LE.off(2);

    boost=false;
    for(int i = 0 ; i <5; i++)
    {
        for(int j = 0 ; j < 3 ; j++)
        {
            display.drawPixel(i+car_x,j+car_position,0x0);
            collision[i+car_x][j+car_position]=0;


        }
    }
    set_car_position();

}

int main(void)
{
    display.init(32,A, B, C,D,E, CLK, LAT, OE, false,64);
    display.begin();


    scheduler_add_task(new terminalTask(PRIORITY_HIGH));


    xTaskCreate(road, (const char*)"panel", STACK_BYTES(500), NULL, PRIORITY_MEDIUM, &road_handle);
    xTaskCreate(crash, (const char*)"crash", STACK_BYTES(1500), NULL, PRIORITY_MEDIUM, &crash_handle);
    xTaskCreate(traffic_task_1, (const char*)"traffic", STACK_BYTES(2048),NULL , PRIORITY_MEDIUM, &traffic_handle);
    xTaskCreate(collision_task, (const char*)"collision", STACK_BYTES(2048), NULL, PRIORITY_MEDIUM, &colllision_handle);
    xTaskCreate(welcome_task, (const char*)"welcome", STACK_BYTES(2048), NULL, PRIORITY_HIGH, &welcome_handle);
    xTaskCreate(level_score, (const char*)"level", STACK_BYTES(2048), NULL, PRIORITY_MEDIUM, &level_handle);


    eint3_enable_port0(0, eint_rising_edge,blank );
    eint3_enable_port0(0, eint_falling_edge, restart_func );
    eint3_enable_port0(1, eint_rising_edge,game_res );
    vTaskSuspend(road_handle);
    vTaskSuspend(traffic_handle);
    vTaskSuspend(level_handle);
    vTaskSuspend(restart_game);
    scheduler_start();
    vTaskStartScheduler();
    return -1;
}
